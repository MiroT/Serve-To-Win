﻿using ServeToWin.DataAccess;
using ServeToWin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ServeToWin
{
    public class Repository<T> where T : BaseModel
    {
        private ServeToWinContext context;
        private DbSet<T> dbSet;

        public Repository(ServeToWinContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public Repository() : this(new ServeToWinContext()) { }

        public List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public List<T> GetAll(Expression<Func<T, bool>> filter = null)
        {
            return dbSet.Where(filter).ToList();
        }

        public T Get(int id)
        {
            return dbSet.Find(id);
        }

        public List<T> GetAllNoTracking()
        {
            return dbSet.AsNoTracking().ToList();
        }

        public void Insert(T model)
        {
            context.Entry(model).State = EntityState.Added;
        }


        public void Update(T model)
        {
            context.Entry(model).State = EntityState.Modified;
        }


        public void Delete(T model)
        {
            context.Entry(model).State = EntityState.Deleted;
        }
    }
}