namespace ServeToWin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        TimesToBeRepeatedPerWeek = c.Byte(nullable: false),
                        Duration = c.Double(nullable: false),
                        TrainerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trainers", t => t.TrainerId, cascadeDelete: true)
                .Index(t => t.TrainerId);
            
            CreateTable(
                "dbo.Trainers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 20),
                        LastName = c.String(nullable: false, maxLength: 20),
                        Age = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        StartOfCareer = c.DateTime(nullable: false),
                        EndOfCareer = c.DateTime(nullable: false),
                        TotalCompetitionsPlayed = c.Short(nullable: false),
                        TotalCompetitionsWon = c.Short(nullable: false),
                        AdditionalInformation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 20),
                        LastName = c.String(nullable: false, maxLength: 20),
                        Email = c.String(nullable: false),
                        Age = c.Int(nullable: false),
                        Phone = c.String(),
                        Address = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        TrainerFirstName = c.String(nullable: false),
                        TrainerLastName = c.String(nullable: false),
                        TrainerId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trainers", t => t.TrainerId)
                .Index(t => t.TrainerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Players", "TrainerId", "dbo.Trainers");
            DropForeignKey("dbo.Exercises", "TrainerId", "dbo.Trainers");
            DropIndex("dbo.Players", new[] { "TrainerId" });
            DropIndex("dbo.Exercises", new[] { "TrainerId" });
            DropTable("dbo.Players");
            DropTable("dbo.Trainers");
            DropTable("dbo.Exercises");
        }
    }
}
