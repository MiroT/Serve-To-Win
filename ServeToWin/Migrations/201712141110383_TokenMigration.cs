namespace ServeToWin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TokenMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GeneratedToken = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        ExpiresAt = c.DateTime(nullable: false),
                        PlayerId = c.Int(),
                        TrainerId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId)
                .ForeignKey("dbo.Trainers", t => t.TrainerId)
                .Index(t => t.PlayerId)
                .Index(t => t.TrainerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tokens", "TrainerId", "dbo.Trainers");
            DropForeignKey("dbo.Tokens", "PlayerId", "dbo.Players");
            DropIndex("dbo.Tokens", new[] { "TrainerId" });
            DropIndex("dbo.Tokens", new[] { "PlayerId" });
            DropTable("dbo.Tokens");
        }
    }
}
