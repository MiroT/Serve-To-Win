namespace ServeToWin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Tokens", "AdminId", c => c.Int());
            CreateIndex("dbo.Tokens", "AdminId");
            AddForeignKey("dbo.Tokens", "AdminId", "dbo.Admins", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tokens", "AdminId", "dbo.Admins");
            DropIndex("dbo.Tokens", new[] { "AdminId" });
            DropColumn("dbo.Tokens", "AdminId");
            DropTable("dbo.Admins");
        }
    }
}
