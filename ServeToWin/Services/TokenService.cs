﻿using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using System.Linq;

namespace ServeToWin.Services
{
    public class TokenService : BaseService<Token>
    {
        private UnitOfWork unitOfWork;
        private IValidation validation;

        public TokenService(UnitOfWork unitOfWork, IValidation validation)
            : base(unitOfWork, validation)
        {
            this.validation = validation;
            this.unitOfWork = unitOfWork;
        }

        public Player LogInPlayer(LogInBindModel logInBindModel)
        {
            return unitOfWork.GetRepo<Player>().GetAll(p => p.Email == logInBindModel.Email 
            && p.Password == logInBindModel.Password).FirstOrDefault();
        }

        public Trainer LogInTrainer(LogInBindModel logInBindModel)
        {
            return unitOfWork.GetRepo<Trainer>().GetAll(p => p.Email == logInBindModel.Email
            && p.Password == logInBindModel.Password).FirstOrDefault();
        }

        public Admin LogInAdmin(LogInBindModel logInBindModel)
        {
            return unitOfWork.GetRepo<Admin>().GetAll(p => p.Email == logInBindModel.Email
            && p.Password == logInBindModel.Password).FirstOrDefault();
        }
    }
}