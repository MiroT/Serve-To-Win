﻿using ServeToWin.DataAccess;
using ServeToWin.Models;
using System.Collections.Generic;

namespace ServeToWin.Services
{
    public abstract class BaseService<T> where T : BaseModel
    {
        private IValidation validation;
        private UnitOfWork unitOfWork;

        public BaseService(UnitOfWork unitOfWork, IValidation validation)
        {
            this.validation = validation;
            this.unitOfWork = unitOfWork;
        }

        public virtual List<T> GetAll()
        {
            return unitOfWork.GetRepo<T>().GetAll();
        }

        public virtual List<T> GetAllNoTrackng()
        {
            return unitOfWork.GetRepo<T>().GetAllNoTracking();
        }

        public virtual T Get(int id)
        {
            return unitOfWork.GetRepo<T>().Get(id);
        }

        public virtual void Insert(T model)
        {
            unitOfWork.GetRepo<T>().Insert(model);
            unitOfWork.Save();
        }

        public virtual void Update(T model)
        {
            unitOfWork.GetRepo<T>().Update(model);
            unitOfWork.Save();
        }

        public virtual void Delete(T model)
        {
            unitOfWork.GetRepo<T>().Delete(model);
            unitOfWork.Save();
        }
    }
}