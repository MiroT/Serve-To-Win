﻿using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ServeToWin.Services
{
    public class PlayerService : BaseService<Player>
    {
        private UnitOfWork unitOfWork;
        private IValidation validation;

        public PlayerService(UnitOfWork unitOfWork, IValidation validation)
            :base(unitOfWork, validation)
        {
            this.unitOfWork = unitOfWork;
            this.validation = validation;
        }

        public void Validation(PlayerBindModel playerBindModel)
        {
            if (!playerBindModel.Password.Any(c => char.IsUpper(c)) || !playerBindModel.Password.Any(c => char.IsLower(c)) || !playerBindModel.Password.Any(c => char.IsDigit(c)))
            {
                validation.Error("Password", "Password has to contain at least one uppercase, lowercase letter and a number.");
            }

            if (playerBindModel.Phone != null)
            {
                if (!Regex.IsMatch(playerBindModel.Phone, @"^(\d|[\s\+\[\]\(\)])+$"))
                {
                    validation.Error("Phone", "Phone is not in the correct format.");
                }
            }

            if (!int.TryParse(playerBindModel.Age, out int resultAge))
            {
                validation.Error("Age", "The age has to be a number.");
            }
            else
            {
                if (resultAge > 80 || resultAge < 5)
                {
                    validation.Error("Age", "The age has to be a number between 5 and 80.");
                }
            }
        }

        //Gets the trainer for the player
        public Trainer GetTrainerForPlayer(PlayerBindModel playerBindModel)
        {
            return unitOfWork.GetRepo<Trainer>().GetAll(t => 
            t.FirstName == playerBindModel.TrainerFirstName && t.LastName == playerBindModel.TrainerLastName)
            .FirstOrDefault();
        }

        public void DeleteAllReferencesWhenDeleted(Player player)
        {
            List<Token> tokens = unitOfWork.GetRepo<Token>().GetAll(t => t.PlayerId == player.Id);

            foreach (Token token in tokens)
            {
                unitOfWork.GetRepo<Token>().Delete(token);
                unitOfWork.Save();
            }
        }
    }
}