﻿using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;

namespace ServeToWin.Services
{
    public class ExerciseService : BaseService<Exercise>
    {
        private UnitOfWork unitOfWork;
        private IValidation validation;

        public ExerciseService(UnitOfWork unitOfWork, IValidation validation)
            : base(unitOfWork, validation)
        {
            this.unitOfWork = unitOfWork;
            this.validation = validation;
        }

        public void Validation(ExerciseBindModel exerciseBindModel)
        {
            if (!double.TryParse(exerciseBindModel.Duration, out double resultDuration))
            {
                validation.Error("Duration", "The duration has to be a number");
            }

            if (!byte.TryParse(exerciseBindModel.TimesToBeRepeatedPerWeek, out byte resultTimesToBeRepeatedPerWeek))
            {
                validation.Error("TimesToBeRepeatedPerWeek", "The repeated times have to be a number");
            }
        }
    }
}