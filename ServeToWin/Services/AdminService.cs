﻿using ServeToWin.DataAccess;
using ServeToWin.Models;
using System.Collections.Generic;

namespace ServeToWin.Services
{
    public class AdminService : BaseService<Admin>
    {
        private UnitOfWork unitOfWork;
        private IValidation validation;

        public AdminService(UnitOfWork unitOfWork, IValidation validation)
            :base(unitOfWork, validation)
        {
            this.unitOfWork = unitOfWork;
            this.validation = validation;
        }

        public void DeleteAllReferencesWhenDeleted(Admin admin)
        {
            List<Token> tokens = unitOfWork.GetRepo<Token>().GetAll(t => t.AdminId == admin.Id);

            foreach (Token token in tokens)
            {
                unitOfWork.GetRepo<Token>().Delete(token);
                unitOfWork.Save();
            }
        }
    }
}