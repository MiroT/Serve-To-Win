﻿namespace ServeToWin.Services
{
    public interface IValidation
    {
        void Error(string key, string msg);
    }
}
