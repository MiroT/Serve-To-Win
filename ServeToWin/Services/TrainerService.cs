﻿using ServeToWin.Models;
using System.Linq;
using ServeToWin.DataAccess;
using ServeToWin.BindModels;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;

namespace ServeToWin.Services
{
    public class TrainerService : BaseService<Trainer>
    {
        private UnitOfWork unitOfWork;
        private IValidation validation;

        public TrainerService(UnitOfWork unitOfWork, IValidation validation) 
            : base(unitOfWork, validation)
        {
            this.unitOfWork = unitOfWork;
            this.validation = validation;
        }

        public void Validation(TrainerBindModel trainerBindModel)
        {
            if (!trainerBindModel.Password.Any(c => char.IsUpper(c)) || !trainerBindModel.Password.Any(c => char.IsLower(c)) || !trainerBindModel.Password.Any(c => char.IsDigit(c)))
            {
                validation.Error("Password", "Password has to contain at least one uppercase, lowercase letter and a number.");
            }

            if (!Regex.IsMatch(trainerBindModel.Phone, @"^(\d|[\s\+\[\]\(\)])+$"))
            {
                validation.Error("Phone", "Phone is not in the correct format.");
            }

            if (!int.TryParse(trainerBindModel.Age, out int resultAge))
            {
                validation.Error("Age", "The age has to be a number.");
            }
            else
            {
                if (resultAge > 80 || resultAge < 20)
                {
                    validation.Error("Age", "The age has to be a number between 5 and 80.");
                }
            }

            if (!DateTime.TryParse(trainerBindModel.StartOfCareer, out DateTime resultStartOfCareer)
                || !DateTime.TryParse(trainerBindModel.EndOfCareer, out DateTime resultEndOfCareer))
            {
                validation.Error("StartOfCareer", "The date has to be a date.");
            }
            else
            {
                if (resultEndOfCareer < resultStartOfCareer)
                {
                    validation.Error("EndOfCareer", "The date for the end of the career has to be further than the one for the start of the career.");
                }
            }

            if (!short.TryParse(trainerBindModel.TotalCompetitionsPlayed, out short resultTotalCompetitionsPlayed)
                || !short.TryParse(trainerBindModel.TotalCompetitionsWon, out short resultTotalCompetitionsWon))
            {
                validation.Error("TotalCompetitionsPlayed", "The played competitions have to be a number.");
            }
            else
            {
                if (resultTotalCompetitionsPlayed < resultTotalCompetitionsWon)
                {
                    validation.Error("resultTotalCompetitionsWon", "The competitions that are won have to be less or equeal to the played ones.");
                }
            }
        }

        public void DeleteAllReferencesWhenDeleted(Trainer trainer)
        {
            List<Token> tokens = unitOfWork.GetRepo<Token>().GetAll(t => t.TrainerId == trainer.Id);

            foreach (Token token in tokens)
            {
                unitOfWork.GetRepo<Token>().Delete(token);
                unitOfWork.Save();
            }
        }

        public void SetAllReferencesToNullWhenDeleted(Trainer trainer)
        {
            List<Player> players = unitOfWork.GetRepo<Player>()
                .GetAll(p => p.TrainerId == trainer.Id)
                .Select(p => 
                {
                    p.TrainerId = null;
                    p.TrainerFirstName = "None";
                    p.TrainerLastName = "None";
                    return p;
                })
                .ToList();

            foreach (Player player in players)
            {
                unitOfWork.GetRepo<Player>().Update(player);
                unitOfWork.Save();
            }
        }
    }
}