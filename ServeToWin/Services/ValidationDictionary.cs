﻿using System.Web.Http.ModelBinding;

namespace ServeToWin.Services
{
    public class ValidationDictionary : IValidation
    {
        private ModelStateDictionary modelState;

        public ValidationDictionary(ModelStateDictionary modelState)
        {
            this.modelState = modelState;
        }

        public void Error(string key, string msg)
        {
            modelState.AddModelError(key, msg);
        }
    }
}