﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ServeToWin.Models
{
    public class Player : BaseModel
    {
        [Required]
        [MaxLength(20)]
        public string FirstName{ get; set; }

        [Required]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Range(5,80)]
        public int Age { get; set; }

        public string Phone { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        public string TrainerFirstName { get; set; }

        [Required]
        public string TrainerLastName { get; set; }

   
        public int? TrainerId { get; set; }
        public virtual Trainer Trainer { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
    }
}