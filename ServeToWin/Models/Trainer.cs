﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ServeToWin.Models
{
    public class Trainer : BaseModel
    {
        [Required]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [Range(20, 80)]
        public int Age { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public DateTime StartOfCareer { get; set; }

        [Required]
        public DateTime EndOfCareer { get; set; }

        [Required]
        public short TotalCompetitionsPlayed { get; set; }

        [Required]
        public short TotalCompetitionsWon { get; set; }

        public string AdditionalInformation { get; set; }


        public virtual ICollection<Player> Players { get; set; }
        public virtual ICollection<Exercise> Exercises { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
    }
}