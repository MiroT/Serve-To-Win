﻿using System.ComponentModel.DataAnnotations;

namespace ServeToWin.Models
{
    public class Exercise : BaseModel
    {
        [Required]
        public string Description { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public byte TimesToBeRepeatedPerWeek { get; set; }

        [Required]
        public double Duration { get; set; }


        public virtual Trainer Trainer { get; set; }
        public int TrainerId { get; set; }
    }
}