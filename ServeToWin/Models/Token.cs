﻿using System;

namespace ServeToWin.Models
{
    public class Token : BaseModel
    {
        public string GeneratedToken { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ExpiresAt { get; set; }

        public int? PlayerId { get; set; }
        public virtual Player Player { get; set; }

        public int? TrainerId { get; set; }
        public virtual Trainer Trainer { get; set; }

        public int? AdminId { get; set; }
        public virtual Admin Admin { get; set; }
    }
}