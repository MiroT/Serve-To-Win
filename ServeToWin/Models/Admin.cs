﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ServeToWin.Models
{
    public class Admin : BaseModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public virtual ICollection<Token> Tokens { get; set; }
    }
}