﻿using ServeToWin.Controllers;
using System.Web.Http;

namespace ServeToWin
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AdminController.CreateFirstAdmin();
        }
    }
}
