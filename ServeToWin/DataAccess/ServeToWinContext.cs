﻿using ServeToWin.Models;
using System.Data.Entity;

namespace ServeToWin.DataAccess
{
    public class ServeToWinContext : DbContext
    {
        public ServeToWinContext()
            :base("ServeToWinDb")
        {
        }

        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Trainer> Trainers { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<Admin> Admins { get; set; }
    }
}