﻿using ServeToWin.Models;
using System;
using System.Collections.Generic;

namespace ServeToWin.DataAccess
{
    public class UnitOfWork : IDisposable
    {
        private ServeToWinContext context = new ServeToWinContext();
        Dictionary<Type, object> dictionary = new Dictionary<Type, object>();

        public Repository<T> GetRepo<T>() where T : BaseModel
        {
            if (!dictionary.ContainsKey(typeof(T)))
            {
                dictionary.Add(typeof(T), new Repository<T>(context));
            }
            return (Repository<T>)dictionary[typeof(T)];
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}