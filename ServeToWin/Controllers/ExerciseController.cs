﻿using ServeToWin.Authentication;
using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using ServeToWin.Services;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;

namespace ServeToWin.Controllers
{
    public class ExerciseController : ApiController
    {
        private StringBuilder errors = new StringBuilder();
        private ExerciseService exerciseService;

        public ExerciseController()
        {
            exerciseService = new ExerciseService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        [AuthorizationActionFilter(false, true)]
        [Route("api/Exercise/Player")]
        public IHttpActionResult GetPlayerExercises()
        {
            List<Exercise> exercises = exerciseService.GetAll().FindAll(e => e.TrainerId == AuthorizationActionFilter.loggedPlayer.TrainerId);

            if (exercises.Count == 0)
            {
                return NotFound();
            }

            List<ExerciseBindModel> exerciseBindModels = new List<ExerciseBindModel>();

            foreach (Exercise exercise in exercises)
            {
                exerciseBindModels.Add(
                    new ExerciseBindModel
                    {
                        Name = exercise.Name,
                        Description = exercise.Description,
                        Duration = exercise.Duration.ToString(),
                        TimesToBeRepeatedPerWeek = exercise.TimesToBeRepeatedPerWeek.ToString()
                    });
            }

            return Ok(exerciseBindModels);
        }

        [AuthorizationActionFilter(true, false)]
        public IHttpActionResult Get()
        {
            List<Exercise> exercises = exerciseService.GetAll();

            if (exercises.Count == 0)
            {
                return NotFound();
            }

            List<ExerciseBindModel> exerciseBindModels = new List<ExerciseBindModel>();

            foreach (Exercise exercise in exercises)
            {
                exerciseBindModels.Add(
                    new ExerciseBindModel
                    {
                        Name = exercise.Name,
                        Description = exercise.Description,
                        Duration = exercise.Duration.ToString(),
                        TimesToBeRepeatedPerWeek = exercise.TimesToBeRepeatedPerWeek.ToString()
                    });
            }

            return Ok(exerciseBindModels);
        }

        [AuthorizationActionFilter(true, false)]
        public IHttpActionResult Get(int id)
        {
            Exercise exercise = exerciseService.Get(id);

            if (exercise == null)
            {
                return NotFound();
            }

            ExerciseBindModel exerciseBindModel = new ExerciseBindModel
            {
                Name = exercise.Name,
                Description = exercise.Description,
                Duration = exercise.Duration.ToString(),
                TimesToBeRepeatedPerWeek = exercise.TimesToBeRepeatedPerWeek.ToString()
            };

            return Ok(exerciseBindModel);
        }

        [AuthorizationActionFilter(true, false)]
        public IHttpActionResult Post(ExerciseBindModel exerciseBindModel)
        {
            exerciseService.Validation(exerciseBindModel);

            if (ModelState.IsValid)
            {
                Exercise exercise = new Exercise
                {
                    Name = exerciseBindModel.Name,
                    Description = exerciseBindModel.Description,
                    Duration = double.Parse(exerciseBindModel.Duration),
                    TimesToBeRepeatedPerWeek = byte.Parse(exerciseBindModel.TimesToBeRepeatedPerWeek),
                    TrainerId = AuthorizationActionFilter.loggedId
                };

                exerciseService.Insert(exercise);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(true, false)]
        [Route("api/Exercise/Trainer/{id}")]
        public IHttpActionResult PutTrainerExercise(ExerciseBindModel exerciseBindModel, int id)
        {
            Exercise currentExercise = exerciseService.GetAllNoTrackng().Find(e => e.Id == id);

            if (currentExercise == null)
            {
                return NotFound();
            }

            if (currentExercise.TrainerId != AuthorizationActionFilter.loggedId)
            {
                return Unauthorized();
            }

            exerciseService.Validation(exerciseBindModel);

            if (ModelState.IsValid)
            {
                Exercise exercise = new Exercise
                {
                    Name = exerciseBindModel.Name,
                    Description = exerciseBindModel.Description,
                    Duration = double.Parse(exerciseBindModel.Duration),
                    TimesToBeRepeatedPerWeek = byte.Parse(exerciseBindModel.TimesToBeRepeatedPerWeek),
                    Id = id,
                    TrainerId = AuthorizationActionFilter.loggedId
                };

                exerciseService.Update(exercise);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    if (error.Key == "id")
                    {
                        continue;
                    }

                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Put(ExerciseBindModel exerciseBindModel, int id)
        {
            Exercise currentExercise = exerciseService.GetAllNoTrackng().Find(e => e.Id == id);

            if (currentExercise == null)
            {
                return NotFound();
            }

            exerciseService.Validation(exerciseBindModel);

            if (ModelState.IsValid)
            {
                Exercise exercise = new Exercise
                {
                    Name = exerciseBindModel.Name,
                    Description = exerciseBindModel.Description,
                    Duration = double.Parse(exerciseBindModel.Duration),
                    TimesToBeRepeatedPerWeek = byte.Parse(exerciseBindModel.TimesToBeRepeatedPerWeek),
                    Id = id,
                    TrainerId = AuthorizationActionFilter.loggedId
                };

                exerciseService.Update(exercise);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    if (error.Key == "id")
                    {
                        continue;
                    }

                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(true, false)]
        [Route("api/Exercise/Trainer/{id}")]
        public IHttpActionResult DeleteTrainerExercise(int id)
        {
            Exercise exercise = exerciseService.Get(id);

            if (exercise == null)
            {
                return NotFound();
            }

            if (exercise.TrainerId != AuthorizationActionFilter.loggedId)
            {
                return Unauthorized();
            }

            exerciseService.Delete(exercise);

            return Ok();
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Delete(int id)
        {
            Exercise exercise = exerciseService.Get(id);

            if (exercise == null)
            {
                return NotFound();
            }

            exerciseService.Delete(exercise);

            return Ok();
        }
    }
}
