﻿using ServeToWin.Authentication;
using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using ServeToWin.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;

namespace ServeToWin.Controllers
{
    public class TrainerController : ApiController
    {
        private StringBuilder errors = new StringBuilder();
        private TrainerService trainerService;

        public TrainerController()
        {
            trainerService = new TrainerService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Get()
        {
            List<Trainer> trainers = trainerService.GetAll();

            if (trainers.Count == 0)
            {
                return NotFound();
            }

            List<TrainerBindModel> trainerBindModels = new List<TrainerBindModel>();

            foreach (Trainer trainer in trainers)
            {
                trainerBindModels.Add(
                    new TrainerBindModel
                    {
                        FirstName = trainer.FirstName,
                        LastName = trainer.LastName,
                        Email = trainer.Email,
                        Password = trainer.Password,
                        Age = trainer.Age.ToString(),
                        StartOfCareer = trainer.StartOfCareer.ToString(),
                        EndOfCareer = trainer.EndOfCareer.ToString(),
                        Address = trainer.Address,
                        Phone = trainer.Phone,
                        TotalCompetitionsPlayed = trainer.TotalCompetitionsPlayed.ToString(),
                        TotalCompetitionsWon = trainer.TotalCompetitionsWon.ToString(),
                        AdditionalInformation = trainer.AdditionalInformation
                    });
            }

            return Ok(trainerBindModels);
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Get(int id)
        {
            Trainer trainer = trainerService.Get(id);

            if (trainer == null)
            {
                return NotFound();
            }

            TrainerBindModel trainerBindModel = new TrainerBindModel
            {
                FirstName = trainer.FirstName,
                LastName = trainer.LastName,
                Email = trainer.Email,
                Password = trainer.Password,
                Age = trainer.Age.ToString(),
                StartOfCareer = trainer.StartOfCareer.ToString(),
                EndOfCareer = trainer.EndOfCareer.ToString(),
                Address = trainer.Address,
                Phone = trainer.Phone,
                TotalCompetitionsPlayed = trainer.TotalCompetitionsPlayed.ToString(),
                TotalCompetitionsWon = trainer.TotalCompetitionsWon.ToString(),
                AdditionalInformation = trainer.AdditionalInformation
            };

            return Ok(trainerBindModel);
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Post(TrainerBindModel trainerBindModel)
        {
            trainerService.Validation(trainerBindModel);

            if (ModelState.IsValid)
            {
                Trainer trainer = new Trainer
                {
                    FirstName = trainerBindModel.FirstName,
                    LastName = trainerBindModel.LastName,
                    Email = trainerBindModel.Email,
                    Password = trainerBindModel.Password,
                    Age = int.Parse(trainerBindModel.Age),
                    StartOfCareer = DateTime.Parse(trainerBindModel.StartOfCareer),
                    EndOfCareer = DateTime.Parse(trainerBindModel.EndOfCareer),
                    Address = trainerBindModel.Address,
                    Phone = trainerBindModel.Phone,
                    TotalCompetitionsPlayed = short.Parse(trainerBindModel.TotalCompetitionsPlayed),
                    TotalCompetitionsWon = short.Parse(trainerBindModel.TotalCompetitionsWon),
                    AdditionalInformation = trainerBindModel.AdditionalInformation
                };

                trainerService.Insert(trainer);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Put(TrainerBindModel trainerBindModel, int id)
        {
            Trainer currentTrainer = trainerService.GetAllNoTrackng().Find(t => t.Id == id);

            if (currentTrainer == null)
            {
                return NotFound();
            }

            trainerService.Validation(trainerBindModel);

            if (ModelState.IsValid)
            {
                Trainer trainer = new Trainer
                {
                    FirstName = trainerBindModel.FirstName,
                    LastName = trainerBindModel.LastName,
                    Email = trainerBindModel.Email,
                    Password = trainerBindModel.Password,
                    Age = int.Parse(trainerBindModel.Age),
                    StartOfCareer = DateTime.Parse(trainerBindModel.StartOfCareer),
                    EndOfCareer = DateTime.Parse(trainerBindModel.EndOfCareer),
                    Address = trainerBindModel.Address,
                    Phone = trainerBindModel.Phone,
                    TotalCompetitionsPlayed = short.Parse(trainerBindModel.TotalCompetitionsPlayed),
                    TotalCompetitionsWon = short.Parse(trainerBindModel.TotalCompetitionsWon),
                    AdditionalInformation = trainerBindModel.AdditionalInformation,
                    Id = id
                };

                trainerService.Update(trainer);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    if (error.Key == "id")
                    {
                        continue;
                    }

                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Delete(int id)
        {
            Trainer trainer = trainerService.Get(id);

            if (trainer == null)
            {
                return NotFound();
            }

            trainerService.DeleteAllReferencesWhenDeleted(trainer);
            trainerService.SetAllReferencesToNullWhenDeleted(trainer);
            trainerService.Delete(trainer);

            return Ok();
        }
    }
}
