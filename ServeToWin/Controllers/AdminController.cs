﻿using ServeToWin.Authentication;
using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using ServeToWin.Services;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;

namespace ServeToWin.Controllers
{
    public class AdminController : ApiController
    {
        private StringBuilder errors = new StringBuilder();
        private AdminService adminService;

        public AdminController()
        {
            adminService = new AdminService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Get()
        {
            List<Admin> admins = adminService.GetAll();

            if (admins.Count == 0)
            {
                return NotFound();
            }

            List<AdminBindModel> adminBindModels = new List<AdminBindModel>();

            foreach (Admin admin in admins)
            {
                adminBindModels.Add(
                    new AdminBindModel
                    {
                        FirstName = admin.FirstName,
                        LastName = admin.LastName,
                        Email = admin.Email,
                        Password = admin.Password
                    });
            }

            return Ok(adminBindModels);
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Get(int id)
        {
            Admin admin = adminService.Get(id);

            if (admin == null)
            {
                return NotFound();
            }

            AdminBindModel adminBindModel = new AdminBindModel
            {
                FirstName = admin.FirstName,
                LastName = admin.LastName,
                Email = admin.Email,
                Password = admin.Password
            };

            return Ok(adminBindModel);
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Post(AdminBindModel adminBindModel)
        {
            if (ModelState.IsValid)
            {
                Admin admin = new Admin
                {
                    FirstName = adminBindModel.FirstName,
                    LastName = adminBindModel.LastName,
                    Email = adminBindModel.Email,
                    Password = adminBindModel.Password
                };

                adminService.Insert(admin);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Put(AdminBindModel adminBindModel, int id)
        {
            Admin currentAdmin = adminService.GetAllNoTrackng().Find(a => a.Id == id);

            if (currentAdmin == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Admin admin = new Admin
                {
                    FirstName = adminBindModel.FirstName,
                    LastName = adminBindModel.LastName,
                    Email = adminBindModel.Email,
                    Password = adminBindModel.Password,
                    Id = id
                };

                adminService.Update(admin);

                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    if (error.Key == "id")
                    {
                        continue;
                    }

                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Delete(int id)
        {
            Admin admin = adminService.Get(id);

            if (admin == null)
            {
                return NotFound();
            }

            adminService.DeleteAllReferencesWhenDeleted(admin);
            adminService.Delete(admin);

            return Ok();
        }

        public static void CreateFirstAdmin()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            Repository<Admin> adminRepository = unitOfWork.GetRepo<Admin>();

            List<Admin> admins = adminRepository.GetAll();

            if (admins.Count == 0)
            {
                Admin admin = new Admin
                {
                    FirstName = "Admin",
                    LastName = "Admin",
                    Email = "admin@gmail.com",
                    Password = "admin"
                };

                adminRepository.Insert(admin);
                unitOfWork.Save();
                unitOfWork.Dispose();
            }
        }
    }
}
