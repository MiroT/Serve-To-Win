﻿using ServeToWin.Authentication;
using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using ServeToWin.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ServeToWin.Controllers
{
    public class ReportController : ApiController
    {
        private TrainerService trainerService;

        public ReportController()
        {
            trainerService = new TrainerService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        [Route("api/Report/MostWins/{top}")]
        [AuthorizationActionFilter(true, true)]
        public IHttpActionResult GetTrainerMostWins(int top)
        {
            List<Trainer> trainers = trainerService.GetAll()
                .OrderByDescending(t => t.TotalCompetitionsWon)
                .Take(top).ToList();

            List<ReportTrainerBindModel> trainerBindModels = new List<ReportTrainerBindModel>();

            foreach (Trainer trainer in trainers)
            {
                trainerBindModels.Add(
                    new ReportTrainerBindModel
                    {
                        FirstName = trainer.FirstName,
                        LastName = trainer.LastName,
                        Age = trainer.Age.ToString(),
                        StartOfCareer = trainer.StartOfCareer.ToString(),
                        EndOfCareer = trainer.EndOfCareer.ToString(),
                        TotalCompetitionsPlayed = trainer.TotalCompetitionsPlayed.ToString(),
                        TotalCompetitionsWon = trainer.TotalCompetitionsWon.ToString(),
                        Phone = trainer.Phone
                    });
            }

            return Ok(trainerBindModels);
        }

        [Route("api/Report/MostMatches/{top}")]
        [AuthorizationActionFilter(true, true)]
        public IHttpActionResult GetTrainerMostMatches(int top)
        {
            List<Trainer> trainers = trainerService.GetAll()
                .OrderByDescending(t => t.TotalCompetitionsPlayed)
                .Take(top).ToList();

            List<ReportTrainerBindModel> trainerBindModels = new List<ReportTrainerBindModel>();

            foreach (Trainer trainer in trainers)
            {
                trainerBindModels.Add(
                    new ReportTrainerBindModel
                    {
                        FirstName = trainer.FirstName,
                        LastName = trainer.LastName,
                        Age = trainer.Age.ToString(),
                        StartOfCareer = trainer.StartOfCareer.ToString(),
                        EndOfCareer = trainer.EndOfCareer.ToString(),
                        TotalCompetitionsPlayed = trainer.TotalCompetitionsPlayed.ToString(),
                        TotalCompetitionsWon = trainer.TotalCompetitionsWon.ToString(),
                        Phone = trainer.Phone
                    });
            }

            return Ok(trainerBindModels);
        }

        [Route("api/Report/MostTimeCareerContinued/{top}")]
        [AuthorizationActionFilter(true, true)]
        public IHttpActionResult GetTrainerMostTimeCareerContinued(int top)
        {
            List<Trainer> trainers = trainerService.GetAll()
                .OrderByDescending(t => (t.EndOfCareer - t.StartOfCareer))
                .Take(top).ToList();

            List<ReportTrainerBindModel> trainerBindModels = new List<ReportTrainerBindModel>();

            foreach (Trainer trainer in trainers)
            {
                trainerBindModels.Add(
                    new ReportTrainerBindModel
                    {
                        FirstName = trainer.FirstName,
                        LastName = trainer.LastName,
                        Age = trainer.Age.ToString(),
                        StartOfCareer = trainer.StartOfCareer.ToString(),
                        EndOfCareer = trainer.EndOfCareer.ToString(),
                        TotalCompetitionsPlayed = trainer.TotalCompetitionsPlayed.ToString(),
                        TotalCompetitionsWon = trainer.TotalCompetitionsWon.ToString(),
                        Phone = trainer.Phone
                    });
            }

            return Ok(trainerBindModels);
        }
    }
}
