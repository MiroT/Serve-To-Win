﻿using ServeToWin.Authentication;
using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using ServeToWin.Services;
using System;
using System.Net;
using System.Security.Cryptography;
using System.Web.Http;

namespace ServeToWin.Controllers
{
    public class AuthenticationController : ApiController
    {
        private TokenService tokenService;
        private RNGCryptoServiceProvider rNGCryptoServiceProvider;

        public AuthenticationController()
        {
            tokenService = new TokenService(new UnitOfWork(), new ValidationDictionary(ModelState));
            rNGCryptoServiceProvider = new RNGCryptoServiceProvider();
        }

        //Player log in
        [Route("api/Authentication/Player")]
        public IHttpActionResult PostPlayer(LogInBindModel logInBindModel)
        {
            Player player = tokenService.LogInPlayer(logInBindModel);
            TokenGenerator tokenGenerator = new TokenGenerator(rNGCryptoServiceProvider);

            if (player != null)
            {
                Token token = new Token
                {
                    CreatedAt = DateTime.Now,
                    ExpiresAt = DateTime.Now.AddMinutes(30),
                    GeneratedToken = tokenGenerator.GetToken(),
                    PlayerId = player.Id
                };

                tokenService.Insert(token);

                return Ok(token.GeneratedToken);
            }

            return Content(HttpStatusCode.NotFound, "Player could not be found.");
        }

        //Trainer log in
        [Route("api/Authentication/Trainer")]
        public IHttpActionResult PostTrainer(LogInBindModel logInBindModel)
        {
            Trainer trainer = tokenService.LogInTrainer(logInBindModel);
            TokenGenerator tokenGenerator = new TokenGenerator(rNGCryptoServiceProvider);

            if (trainer != null)
            {
                Token token = new Token
                {
                    CreatedAt = DateTime.Now,
                    ExpiresAt = DateTime.Now.AddMinutes(30),
                    GeneratedToken = tokenGenerator.GetToken(),
                    TrainerId = trainer.Id
                };

                tokenService.Insert(token);

                return Ok(token.GeneratedToken);
            }

            return Content(HttpStatusCode.NotFound, "Trainer could not be found.");
        }

        //Admin log in
        [Route("api/Authentication/Admin")]
        public IHttpActionResult PostAdmin(LogInBindModel logInBindModel)
        {
            Admin admin = tokenService.LogInAdmin(logInBindModel);
            TokenGenerator tokenGenerator = new TokenGenerator(rNGCryptoServiceProvider);

            if (admin != null)
            {
                Token token = new Token
                {
                    CreatedAt = DateTime.Now,
                    ExpiresAt = DateTime.Now.AddMinutes(30),
                    GeneratedToken = tokenGenerator.GetToken(),
                    AdminId = admin.Id
                };

                tokenService.Insert(token);

                return Ok(token.GeneratedToken);
            }

            return Content(HttpStatusCode.NotFound, "Admin could not be found.");
        }
    }
}
