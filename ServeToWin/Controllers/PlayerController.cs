﻿using ServeToWin.Authentication;
using ServeToWin.BindModels;
using ServeToWin.DataAccess;
using ServeToWin.Models;
using ServeToWin.Services;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web.Http;

namespace ServeToWin.Controllers
{
    public class PlayerController : ApiController
    {
        private StringBuilder errors = new StringBuilder();
        private PlayerService playerService;

        public PlayerController()
        {
            playerService = new PlayerService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        [AuthorizationActionFilter(true, false)]
        [Route("api/Player/Trainer")]
        public IHttpActionResult GetTrainerPlayers()
        {
            List<Player> players = playerService.GetAll().FindAll(p => p.TrainerId == AuthorizationActionFilter.loggedId);

            if (players.Count == 0)
            {
                return NotFound();
            }

            List<PlayerBindModel> playerBindModels = new List<PlayerBindModel>();

            foreach (Player player in players)
            {
                playerBindModels.Add(
                    new PlayerBindModel
                    {
                        FirstName = player.FirstName,
                        LastName = player.LastName,
                        Email = player.Email,
                        Password = player.Password,
                        Age = player.Age.ToString(),
                        Address = player.Address,
                        Phone = player.Phone,
                        TrainerFirstName = player.TrainerFirstName,
                        TrainerLastName = player.TrainerLastName
                    });
            }

            return Ok(playerBindModels);
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Get()
        {
            List<Player> players = playerService.GetAll();

            if (players.Count == 0)
            {
                return NotFound();
            }

            List<PlayerBindModel> playerBindModels = new List<PlayerBindModel>();

            foreach (Player player in players)
            {
                playerBindModels.Add(
                    new PlayerBindModel
                    {
                        FirstName = player.FirstName,
                        LastName = player.LastName,
                        Email = player.Email,
                        Password = player.Password,
                        Age = player.Age.ToString(),
                        Address = player.Address,
                        Phone = player.Phone,
                        TrainerFirstName = player.TrainerFirstName,
                        TrainerLastName = player.TrainerLastName
                    });
            }

            return Ok(playerBindModels);
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Get(int id)
        {
            Player player = playerService.Get(id);

            if (player == null)
            {
                return NotFound();
            }

            PlayerBindModel playerBindModel = new PlayerBindModel
            {
                FirstName = player.FirstName,
                LastName = player.LastName,
                Email = player.Email,
                Password = player.Password,
                Age = player.Age.ToString(),
                Address = player.Address,
                Phone = player.Phone,
                TrainerFirstName = player.TrainerFirstName,
                TrainerLastName = player.TrainerLastName
            };

            return Ok(playerBindModel);
        }

        public IHttpActionResult Post(PlayerBindModel playerBindModel)
        {
            playerService.Validation(playerBindModel);

            if (ModelState.IsValid)
            {
                Player player = new Player
                {
                    FirstName = playerBindModel.FirstName,
                    LastName = playerBindModel.LastName,
                    Email = playerBindModel.Email,
                    Password = playerBindModel.Password,
                    Age = int.Parse(playerBindModel.Age),
                    Address = playerBindModel.Address,
                    Phone = playerBindModel.Phone,
                    TrainerFirstName = playerBindModel.TrainerFirstName,
                    TrainerLastName = playerBindModel.TrainerLastName,
                };

                Trainer trainer = playerService.GetTrainerForPlayer(playerBindModel);

                if (trainer == null)
                {
                    return Content(HttpStatusCode.NotFound, "Trainer could not be found.");
                }

                player.TrainerId = trainer.Id;

                playerService.Insert(player);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Put(PlayerBindModel playerBindModel, int id)
        {
            Player currentPlayer = playerService.GetAllNoTrackng().Find(p => p.Id == id);

            if (currentPlayer == null)
            {
                return NotFound();
            }

            playerService.Validation(playerBindModel);

            if (ModelState.IsValid)
            {
                Player player = new Player
                {
                    FirstName = playerBindModel.FirstName,
                    LastName = playerBindModel.LastName,
                    Email = playerBindModel.Email,
                    Password = playerBindModel.Password,
                    Age = int.Parse(playerBindModel.Age),
                    Address = playerBindModel.Address,
                    Phone = playerBindModel.Phone,
                    TrainerFirstName = playerBindModel.TrainerFirstName,
                    TrainerLastName = playerBindModel.TrainerLastName,
                    Id = id
                };

                Trainer trainer = playerService.GetTrainerForPlayer(playerBindModel);

                if (trainer == null)
                {
                    return Content(HttpStatusCode.NotFound, "Trainer could not be found.");
                }

                player.TrainerId = trainer.Id;

                playerService.Update(player);
                return Ok();
            }
            else
            {
                foreach (var error in ModelState)
                {
                    if (error.Key == "id")
                    {
                        continue;
                    }

                    errors.Append(string.Format($"{error.Value.Errors[0].ErrorMessage}; "));
                }

                return BadRequest($"A mistake has occured: {errors}");
            }
        }

        [AuthorizationActionFilter(false, false)]
        public IHttpActionResult Delete(int id)
        {
            Player player = playerService.Get(id);

            if (player == null)
            {
                return NotFound();
            }

            playerService.DeleteAllReferencesWhenDeleted(player);
            playerService.Delete(player);

            return Ok();
        }
    }
}
