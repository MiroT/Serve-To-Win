﻿namespace ServeToWin.BindModels
{
    public class ReportTrainerBindModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Age { get; set; }

        public string Phone { get; set; }

        public string StartOfCareer { get; set; }

        public string EndOfCareer { get; set; }

        public string TotalCompetitionsPlayed { get; set; }

        public string TotalCompetitionsWon { get; set; }
    }
}