﻿using System.ComponentModel.DataAnnotations;

namespace ServeToWin.BindModels
{
    public class ExerciseBindModel
    {
        [Required]
        public string Description { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string TimesToBeRepeatedPerWeek { get; set; }

        [Required]
        public string Duration { get; set; }
    }
}