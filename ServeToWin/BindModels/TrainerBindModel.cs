﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ServeToWin.BindModels
{
    public class TrainerBindModel
    {
        [Required]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        public string Age { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string StartOfCareer { get; set; }

        [Required]
        public string EndOfCareer { get; set; }

        [Required]
        public string TotalCompetitionsPlayed { get; set; }

        [Required]
        public string TotalCompetitionsWon { get; set; }

        public string AdditionalInformation { get; set; }
    }
}