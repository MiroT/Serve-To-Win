﻿namespace ServeToWin.BindModels
{
    public class LogInBindModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}