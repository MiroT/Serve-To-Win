﻿using ServeToWin.DataAccess;
using ServeToWin.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ServeToWin.Authentication
{
    public class AuthorizationActionFilter : ActionFilterAttribute, IActionFilter
    {
        private bool trainer;
        private bool player;
        public static int loggedId;

        public static Player loggedPlayer;
        public static Trainer loggedTrainer;

        public AuthorizationActionFilter(bool trainer, bool player)
        {
            this.trainer = trainer;
            this.player = player;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            HttpRequestMessage request = actionContext.Request;

            if (request.Headers.Authorization == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.NotFound, "Token is not entered.");
                return;
            }

            string token = request.Headers.Authorization.ToString();

            UnitOfWork unitOfWork = new UnitOfWork();
            Token currentToken = unitOfWork.GetRepo<Token>().GetAll(t => t.GeneratedToken == token).FirstOrDefault();

            if (currentToken != null && currentToken.ExpiresAt > DateTime.Now)
            {
                if (currentToken.PlayerId != null)
                {
                    if (!player)
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    }

                    loggedId = (int)currentToken.PlayerId;
                    loggedPlayer = currentToken.Player;
                    unitOfWork.Dispose();
                    return;
                }

                if (currentToken.TrainerId != null)
                {
                    if (!trainer)
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    }

                    loggedId = (int)currentToken.TrainerId;
                    loggedTrainer = currentToken.Trainer;
                    unitOfWork.Dispose();
                    return;
                }

                if (currentToken.AdminId != null)
                {
                    loggedId = (int)currentToken.AdminId;
                    unitOfWork.Dispose();
                    return;
                }
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.NotFound, "Token could not be found or has expired, please generate a new one.");
                unitOfWork.Dispose();
                return;
            }
        }
    }
}