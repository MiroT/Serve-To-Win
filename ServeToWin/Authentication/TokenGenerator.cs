﻿using System.Security.Cryptography;
using System.Text;

namespace ServeToWin.Authentication
{
    public class TokenGenerator
    {
        private RNGCryptoServiceProvider crypt;

        public TokenGenerator(RNGCryptoServiceProvider crypt)
        {
            this.crypt = crypt;
        }

        public string GetToken(int maxSize = 20)
        {
            char[] chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            byte[] data = new byte[1];

            using (crypt)
            {
                crypt.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypt.GetNonZeroBytes(data);
            }

            StringBuilder resultStringBuilder = new StringBuilder(maxSize);

            foreach (byte symbol in data)
            {
                resultStringBuilder.Append(chars[symbol % (chars.Length)]);
            }

            return resultStringBuilder.ToString();
        }
    }
}